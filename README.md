# Gunicorn Docker Image

A Gunicorn Docker image that ensures reproducibility and hash checking for Gunicorn and its dependencies, to be used to build other images.

## Usage

Use this image to build another image, where you require access to Gunicorn (usually in a stage in multistage images):

`FROM registry.gitlab.com/nevrona/public/gunicorn-docker:<version>`

## Versions

This project will publish versions equal to Gunicorn versions so it's easier to use. So for Gunicorn v1.2.3, use `registry.gitlab.com/nevrona/public/gunicorn-docker:1.2.3`.

## License

*Gunicorn docker* is made by [Nevrona S. A.](https://nevrona.org) under `MPL-2.0`. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

**Neither the name of Nevrona S. A. nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.**
